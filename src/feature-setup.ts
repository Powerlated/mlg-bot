import paperTrail from "./features/primary/paper-trail";
import deafenNotif from "./features/primary/deafen-notif";

export default function setupFeatures() {
    deafenNotif();
    paperTrail();
}