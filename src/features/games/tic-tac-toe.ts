import { randomElement } from "../../commands/fun/bitmoji";

const botUtil = require('../../utils/util');
const bot = require('../..').bot;
const _ = require('lodash');

const xSymbol = 'X';
const oSymbol = 'O';

const util = require('../../utils/util');

const gameEndSeconds = 30;

let channelDeleteTimeout;

export default class TicTacToeGame {
	ongoingGames: any;
	gameName: string;
	gameChannel: any;
	player1: any;
	turn: any;
	spacesLeft: number;
	tie: boolean;
	ticTacToeBoard: string[][];
	timer: any;
	msgListener: any;
	msgHandler: (msg: any) => void;
	player2: any;
	ready: boolean;
	playerChickenedOut: any;
	winner: any;
	ended: boolean;

	constructor(gameChannel, player1, ongoingGames) {
		/** @type {Map} */
		this.ongoingGames = ongoingGames;
		this.gameName = 'Tic Tac Toe';

		this.gameChannel = gameChannel;
		this.player1 = player1;
		this.turn = player1;

		this.spacesLeft = 9;
		this.tie = false;

		this.ticTacToeBoard = Array.from({ length: 3 }, (array) => Array.from({ length: 3 }, (value) => ' '));
	}

	gameGuide() {
		let char1 = randomElement(['a', 'b', 'c']);
		let char2 = randomElement(['1', '2', '3']);

		var gameGuide = '';

		gameGuide += '```\n';
		gameGuide += `--- How to Play ---\n`;
		gameGuide += `Use <column><row> to mark your position. Example: ${char1 + char2}\n`;
		gameGuide += `When a victory is detected, the game will automatically end.\n\n`;
		gameGuide += `If you feel scared, type \`ditch\` to chicken out.`;
		gameGuide += '```\n';

		return gameGuide;
	}

	sendGameGuide() {
		return this.gameChannel.send(this.gameGuide());
	}

	gameStatus() {
		var tttb = this.ticTacToeBoard;

		var gameStatus = '';

		gameStatus += '```\n';
		gameStatus += `--- Tic Tac Toe ---\n`;
		gameStatus += `     A   B   C     \n`;
		gameStatus += `   1 ${tttb[0][0]} │ ${tttb[0][1]} │ ${tttb[0][2]}     \n`;
		gameStatus += `    ───┼───┼───    \n`;
		gameStatus += `   2 ${tttb[1][0]} │ ${tttb[1][1]} │ ${tttb[1][2]}     \n`;
		gameStatus += `    ───┼───┼───    \n`;
		gameStatus += `   3 ${tttb[2][0]} │ ${tttb[2][1]} │ ${tttb[2][2]}     \n`;
		gameStatus += `                   \n`;

		this.spacesLeft = 0;
		tttb.forEach((array) => {
			array.forEach((space) => {
				if (space == ' ') {
					this.spacesLeft++;
				}
			});
		});

		gameStatus += `   Spaces left: ${this.spacesLeft}  \n`;
		gameStatus += `                   \n`;
		gameStatus += '```\n';

		return gameStatus;
	}

	sendGameStatus() {
		return this.gameChannel.send(this.gameStatus());
	}

	/** Columns and rows are zero indexed from the top left. */
	placeSymbol(player, column, row) {
		var symbol;
		if (player == this.player1) {
			symbol = 'x';
		} else {
			symbol = 'o';
		}

		if (column > 2 || row > 2) return;

		this.ticTacToeBoard[column][row] = symbol;
	}

	/** True if successful. */
	placeSymbolSafe(player, column, row) {
		if (this.turn != player) return false;
		var cell = this.ticTacToeBoard[column][row];
		if (cell != ' ') return false;
		this.placeSymbol(player, column, row);
		return true;
	}

	resetTimer() {
		if (this.timer != null) {
			clearTimeout(this.timer);
		}
		this.timer = setTimeout(
			(channel) => {
				channel.send(`No move after ${gameEndSeconds} seconds, ending game...`);
				this.end(channel);
			},
			gameEndSeconds * 1000,
			this.gameChannel
		);
	}

	checkWinner(symbol, applyCheckmarks) {
		var s = symbol;
		var b = this.ticTacToeBoard;
		var c = '+';

		if (
			// Horizontal
			(b[0][0] == s && b[0][1] == s && b[0][2] == s) ||
			(b[1][0] == s && b[1][1] == s && b[1][2] == s) ||
			(b[2][0] == s && b[2][1] == s && b[2][2] == s) ||
			// Vertical
			(b[0][0] == s && b[1][0] == s && b[2][0] == s) ||
			(b[0][1] == s && b[1][1] == s && b[2][1] == s) ||
			(b[0][2] == s && b[1][2] == s && b[2][2] == s) ||
			// Diagonal
			(b[0][0] == s && b[1][1] == s && b[2][2] == s) ||
			(b[0][2] == s && b[1][1] == s && b[2][0] == s)
		) {
			if (applyCheckmarks) {
				if (b[0][0] == s && b[0][1] == s && b[0][2] == s) {
					b[0][0] = c;
					b[0][1] = c;
					b[0][2] = c;
				} else if (b[1][0] == s && b[1][1] == s && b[1][2] == s) {
					b[1][0] = c;
					b[1][1] = c;
					b[1][2] = c;
				} else if (b[2][0] == s && b[2][1] == s && b[2][2] == s) {
					b[2][0] = c;
					b[2][1] = c;
					b[2][2] = c;
				} else if (b[0][0] == s && b[1][0] == s && b[2][0] == s) {
					b[0][0] = c;
					b[1][0] = c;
					b[2][0] = c;
				} else if (b[0][1] == s && b[1][1] == s && b[2][1] == s) {
					b[0][1] = c;
					b[1][1] = c;
					b[2][1] = c;
				} else if (b[0][2] == s && b[1][2] == s && b[2][2] == s) {
					b[0][2] = c;
					b[1][2] = c;
					b[2][2] = c;
				} else if (b[0][0] == s && b[1][1] == s && b[2][2] == s) {
					b[0][0] = c;
					b[1][1] = c;
					b[2][2] = c;
				} else if (b[0][2] == s && b[1][1] == s && b[2][0] == s) {
					b[0][2] = c;
					b[1][1] = c;
					b[2][0] = c;
				}
			}
			return true;
		}
		return false;
	}

	start() {
		var tttc = this.gameChannel;

		if (channelDeleteTimeout) {
			clearTimeout(channelDeleteTimeout);
			tttc.send(`Channel delete canceled.\n`);
		}

		tttc.send(`A tic-tac-toe game has been started by <@${this.player1.id}>!`);
		tttc.send('Waiting for a player... Type `join` to join the game.');

		this.resetTimer();
		this.handleMessages();
	}

	handleMessages() {
		const bot = require('../..').bot;
		this.msgListener = bot.on(
			'message',
			(this.msgHandler = (msg) => {
				//if (this.ended) return;
				var tttc = this.gameChannel;
				if (msg.channel == tttc) {
					if (this.spacesLeft == 0) {
						this.gameChannel.send('There are no spaces left!');
						this.tie = true;
						this.end(this.gameChannel);
						return;
					}
					if (msg.content.toLowerCase() == 'join'.toLowerCase() && this.player2 == null) {
						this.player2 = msg.author;
						tttc.send(`<@${msg.author.id}> has joined the game as Player 2!`);

						this.sendGameStatus().then(() =>
							this.sendGameGuide().then(
								this.gameChannel.send(`<@${this.player1.id}>, your turn!`).then((this.ready = true))
							)
						);

						this.resetTimer();
						return;
					}

					if (this.player2) {
						if (msg.content.toLowerCase() == 'ditch') {
							this.playerChickenedOut = msg.author;
							this.end(tttc);
						}
					}

					if (this.ready && msg.author.id != bot.user.id) {
						if (msg.author != this.player1 && msg.author != this.player2) {
							return;
						}
					}

					if (msg.content.length == 2 && this.ready) {
						/** @type {Array} */
						const text = msg.content.toLowerCase().split('');
						var moves = [];
						if (
							(text[0] == 'a' || text[0] == 'b' || text[0] == 'c') &&
							(text[1] == '1' || text[1] == '2' || text[1] == '3')
						) {
							switch (text[0]) {
								case 'a':
									moves[1] = 0;
									break;
								case 'b':
									moves[1] = 1;
									break;
								case 'c':
									moves[1] = 2;
									break;
							}
							switch (text[1]) {
								case '1':
									moves[0] = 0;
									break;
								case '2':
									moves[0] = 1;
									break;
								case '3':
									moves[0] = 2;
									break;
							}
							if (text[0] == null || text[1] == null) return;
							if (this.placeSymbolSafe(this.turn, moves[0], moves[1])) {
								if (this.checkWinner('x', true)) {
									this.sendGameStatus();
									this.gameChannel.send(`Congratulations to <@${this.player1.id}> for the win!`);
									this.winner = this.player1;
									this.end(this.gameChannel);
									return;
								} else if (this.checkWinner('o', true)) {
									this.sendGameStatus();
									this.gameChannel.send(`Congratulations to <@${this.player2.id}> for the win!`);
									this.winner = this.player2;
									this.end(this.gameChannel);
									return;
								}

								this.sendGameStatus().then(() => {
									if (this.turn == this.player1) {
										this.turn = this.player2;
										this.gameChannel.send(`<@${this.player2.id}>, your turn!`);
									} else {
										this.turn = this.player1;
										this.gameChannel.send(`<@${this.player1.id}>, your turn!`);
									}
								});
								this.resetTimer();
							} else {
								msg.reply(
									`that square has already been filled by ${this.ticTacToeBoard[moves[0]][moves[1]] ==
										'x'
										? this.player1
										: this.player2}.`
								);
							}
						}
					}
				}
			})
		);
	}

	end(channel) {
		const bot = require('../..').bot;

		this.ongoingGames.delete(channel);
		clearTimeout(this.timer);
		bot.removeListener('message', this.msgHandler);

		this.ended = true;

		if (this.winner != null) {
			channel.send(`The game has ended with <@${this.winner.id}> being the winner!`);
		} else if (this.tie == true) {
			channel.send('The game has ended in a tie!');
		} else if (this.playerChickenedOut) {
			channel.send(`${this.playerChickenedOut} has chickened out of the game!`);
		} else {
			channel.send('The game has ended with no winner.');
		}

		channel.send('Game finished. Deleting channel in 15 seconds.');

		channelDeleteTimeout = setTimeout(() => {
			channel.delete().catch((err) => {
				channel.send(
					`Hey, ${channel.guild
						.owner}, could you please possibly raise my permissions to the highest possible? Just look at this error: ${err}`
				);
			});
		}, 15000);
	}
};
