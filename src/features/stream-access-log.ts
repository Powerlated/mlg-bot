import { getChannelFromGuild } from "../utils/util";
import { bot, bestGuildId } from "..";

const ts = require("tail-stream");
const logger = require("winston");

var accessLogChannel = getChannelFromGuild(
  bestGuildId,
  "access-log"
);

if (accessLogChannel != null) {
  var tstream = ts.createReadStream("/var/log/nginx/access.log", {
    beginAt: "end",
    onMove: "stay",
    detectTruncate: true,
    onTruncate: "end",
    endOnError: false
  });

  tstream.on("data", data => {
    accessLogChannel.send(`\`${data.toString()}\``);
  });
} else {
  logger.info(
    `access-log text channel not found! Use '#!mlg access-log' to recheck.`
  );
}
