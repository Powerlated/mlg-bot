import { GuildChannel, GuildMember, User, Guild } from 'discord.js';
//@ts-check

import { util } from "discord.js-commando";
import * as Discord from 'discord.js';
import { bot } from "../..";
import { getChannelFromGuild } from "../../utils/util";

// const serverId = "448658607140110336";
const serverId = "243513093538316288";
/** @return {(import('discord.js').TextChannel | any)} */
function getChannel() {
  try {
    var c = getChannelFromGuild(serverId, "bot-output");
  } catch (e) { }
  if (c) {
    return c;
  } else {
    // Return stubbed send function
    return {
      send() { }
    };
  }
}

function fm(member: GuildMember) {
  return `${member.user.username}#${member.user.discriminator}<${member.id}>`;
}

function fu(user: User) {
  return `${user.username}#${user.discriminator}<${user.id}>`;
}

function t(guild: Guild) {
  let d = new Date();
  return `┏ __**Server**__: \`${guild.name}<${guild.id}>\` ┓
**[${d.getDate() +
    1}/${d.getDate()}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}:${
    d.getSeconds().toString().length == 1
      ? "0" + d.getSeconds()
      : d.getSeconds()
    }]** `;
}

export default function () {
  console.log("Initializing paper-trail");

  bot.on("guildMemberUpdate", (oldUser, newUser) => {
    let c = getChannel();
    if (
      oldUser instanceof Discord.GuildMember &&
      newUser instanceof Discord.GuildChannel
    ) {
      if (oldUser.user.username != newUser.user.username) {
        c.send(
          t(newUser.guild) +
          `\`${fm(oldUser)}\` changed their username to ${fm(newUser)}`
        );
      }
      if (oldUser.user.avatar != newUser.user.avatar) {
        c.send(
          t(newUser.guild) +
          `\`${fm(oldUser)}\` changed their profile picture to ${fm(newUser)}`
        );
      }
    }
  });

  bot.on("guildMemberUpdate", (oldUser, newUser) => {
    let c = getChannel();

    if (
      oldUser.nickname &&
      newUser.nickname &&
      oldUser.nickname != newUser.nickname
    ) {
      c.send(
        t(newUser.guild) +
        `\`${fm(newUser)}\` had their nickname changed from \`${
        oldUser.nickname
        }\`to \`${newUser.nickname}\``
      );
    } else if (!newUser.nickname) {
      c.send(
        t(newUser.guild) +
        `\`${fm(oldUser)}\` had their nickname \`${
        oldUser.nickname
        }\` removed.`
      );
    } else if (!oldUser.nickname) {
      c.send(
        t(newUser.guild) +
        `\`${fm(newUser)}\` had their nickname set to \`${
        newUser.nickname
        }\`.`
      );
    }
  });

  bot.on("voiceStateUpdate", (oldState, newState) => {
    let c = getChannel();

    if (!oldState.channel) {
      c.send(
        t(newState.guild) +
        `\`${fm(newState.member)}\` entered \`${newState.channel.name}\`.`
      );
    } else if (
      newState.channel &&
      newState.channel != oldState.channel
    ) {
      c.send(
        t(newState.guild) +
        `\`${fm(newState.member)}\` moved from \`${
        oldState.channel.name
        }\` to \`${newState.channel.name}\`.`
      );
    } else if (!oldState.serverMute && newState.serverMute) {
      c.send(t(newState.guild) + `\`${fm(newState.member)}\` was server muted.`);
    } else if (!oldState.serverDeaf && newState.serverDeaf) {
      c.send(t(newState.guild) + `\`${fm(newState.member)}\` was server deafened.`);
    } else if (oldState.serverMute && !newState.serverMute) {
      c.send(t(newState.guild) + `\`${fm(newState.member)}\` was server unmuted.`);
    } else if (oldState.serverDeaf && !newState.serverDeaf) {
      c.send(
        t(newState.guild) + `\`${fm(newState.member)}\` was server undeafened.`
      );
    } else if (!oldState.mute && newState.mute) {
      c.send(t(newState.guild) + `\`${fm(newState.member)}\` muted themselves.`);
      if (!oldState.deaf && newState.deaf) {
        c.send(
          t(newState.guild) + `\`${fm(newState.member)}\` deafened themselves.`
        );
      }
    } else if (oldState.mute && !newState.mute) {
      c.send(t(newState.guild) + `\`${fm(newState.member)}\` unmuted themselves.`);
    } else if (oldState.deaf && !newState.deaf) {
      c.send(
        t(newState.guild) + `\`${fm(newState.member)}\` undeafened themselves.`
      );
    } else if (oldState && !newState) {
      c.send(
        t(newState.guild) + `\`${fm(oldState.member)}\` left \`${fm(newState.member)}\`.`
      );
    }
  });

  bot.on("message", msg => {
    if (msg.author.bot) return;
    if (msg.channel instanceof GuildChannel)
      getChannel().send(
        t(msg.guild) +
        `Message by \`${msg.author.username}#${msg.author.discriminator}<${msg.author.id}>\` with content \`${msg.content}\` sent in channel \`${msg.channel.name}\`.`
      );
  });
  bot.on("messageDelete", msg => {
    if (msg.channel instanceof GuildChannel)
      getChannel().send(
        t(msg.guild) +
        `Message by \`${msg.author.username}#${msg.author.discriminator}<${msg.author.id}>\` with content \`${msg.content}\` deleted in channel \`${msg.channel.name}\`.`
      );
  });
  bot.on("messageUpdate", (oldMessage, newMessage) => {
    if (oldMessage.content != newMessage.content) {
      getChannel().send(
        t(newMessage.guild) +
        `Message by ${fu(oldMessage.author)} with content \`${
        oldMessage.content
        }\` updated to content \`${newMessage.content}\``
      );
    }
  });

  bot.on("roleCreate", role => {
    getChannel().send(t(role.guild) + `Role ${role} created.`);
  });
  bot.on("roleDelete", role => {
    getChannel().send(t(role.guild) + `Role ${role} deleted.`);
  });
  bot.on("roleUpdate", role => {
    getChannel().send(t(role.guild) + `Role ${role} updated.`);
  });

  bot.on("guildMemberAdd", member => {
    getChannel().send(t(member.guild) + `\`${fm(member)}\` joined the server.`);
  });
  bot.on("guildMemberRemove", member => {
    getChannel().send(t(member.guild) + `\`${fm(member)}\` left the server.`);
  });

  bot.on("guildMemberSpeaking", (member, speaking) => {
    getChannel().send(
      t(member.guild) +
      `\`${fm(member)}\` ${speaking ? "started" : "stopped"} speaking in \`${
      member.voice.channel.name
      }\`.`
    );
  });

  let lastPresenceUpdate = {};
  bot.on("presenceUpdate", (oldMember, newMember) => {
    const statuses = {
      online: "Online",
      offline: "Offline / Invisible",
      dnd: "Do Not Disturb",
      idle: "Idle"
    };

    if (!lastPresenceUpdate[newMember.id]) {
      lastPresenceUpdate[newMember.id] = new Date().getTime() - 10000;
    }

    if (newMember.presence && oldMember.presence) {
      if (
        newMember.presence.status != oldMember.presence.status &&
        new Date().getTime() - 1 * 1000 > lastPresenceUpdate[newMember.id]
      ) {
        getChannel().send(
          t(newMember.guild) +
          `\`${fm(newMember)}\` updated presence to \`${
          statuses[newMember.presence.status]
          }\`.`
        );
        lastPresenceUpdate[newMember.id] = new Date().getTime();
      }
    }
  });

  bot.on("channelCreate", channel => {
    if (channel instanceof Discord.GuildChannel) {
      getChannel().send(
        t(channel.guild) + `\`${channel.name}\` channel created.`
      );
    }
  });
  bot.on("channelDelete", channel => {
    if (channel instanceof Discord.GuildChannel) {
      getChannel().send(
        t(channel.guild) + `\`${channel.name}\` channel deleted.`
      );
    }
  });
  // bot.on("channelUpdate", (oldChannel, newChannel) => {
  //     getChannel().send();
  // });
};
