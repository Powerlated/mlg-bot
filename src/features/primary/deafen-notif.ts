import { bot } from "../..";

const followId = "190195717531238400";
const map = {
    "391340239009873922": "r",
    "189487132392292352": "d",
    "365325663265226753": "z",
    "639223453148250124": "za2",
    "190195717531238400": "b",
    "295960935963557898": "mj",
    "488789794676146177": "h",
    "357252943411675148": "m"
};
let queue = [];
export default function () {
    console.log("Initializing deafen-notif");

    bot.on("voiceStateUpdate", async (oldS, s) => {
        // if (newM.id == followId) {
        //     newM.voiceChannel.join();
        // }
        // if (newM.selfDeaf) {
        //     console.log(`${newM.user.username}#${newM.user.discriminator}<${newM.user.id}> deafened.`);
        //     if (map[newM.user.id]) queue.push({user: newM, ref: map[newM.user.id]});
        // }
        if (s.id == followId) {
            if (s.channel) {
                let c = await s.channel.join();
                if (s.selfDeaf) {
                    console.log("Target deafened.");
                    if (c.dispatcher) c.dispatcher.end();
                    let d = c.play("src/assets/m_deafen.ogg");
                    d.on("end", () => d.end());
                }
            }
        }
    });
};

setInterval(function play() {
    let a = queue.shift();
    if (a && a.user.guild.voiceConnection) {
        let c = a.user.guild.voiceConnection;
        if (c.dispatcher) c.dispatcher.end();
        let d = c.playFile(`src/assets/${a.ref}_deafen.ogg`);
        d.on("end", () => d.end());
    }
}, 500);
