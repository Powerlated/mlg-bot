import { bot } from "../..";

export default function() {
    const low = require("lowdb");
    const FileSync = require("lowdb/adapters/FileSync");

    const adapter = new FileSync("data/msgs.json");
    const db = low(adapter);

    
    

    db.defaults({}).write();

    bot.on("message", msg => {
        if (msg.author.bot || msg.author.id == bot.user.id) return;

        if (msg.channel.type != "text") return;

        if (msg.content.includes(bot.commandPrefix)) return;

        db.set(msg.id, {
            author: {
                id: msg.author.id,
                name: msg.author.username,
                discriminator: msg.author.discriminator
            },
            guild: { id: msg.guild.id },
            channel: { id: msg.channel.id },
            createdTimestamp: msg.createdTimestamp,
            id: msg.id,
            content: msg.content,
            cleanContent: msg.cleanContent,
            attachments: msg.attachments,
            type: msg.type
        }).write();
    });

};
