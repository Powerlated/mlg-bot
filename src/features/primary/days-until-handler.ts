import * as  fs from "fs";
import { datadir, bot } from "../..";
import { TextChannel } from "discord.js";

export default function () {
    let storageFile = datadir + "days-until.json";

    function run() {
        bot.guilds.cache.forEach(g => {
            if (g.id != "558019594686824460") return;

            let storage = [];
            let n = "sdfkjhsdfj"
            // let n = -8;
            console.log(`Guild name: ${g.name}`);
            let announcementsChannel = g.channels.cache.find(c => c.name == "announcements");
            if (!announcementsChannel) return;

            if (!fs.existsSync(storageFile)) {
                fs.writeFileSync(storageFile, JSON.stringify(storage));
            }

            fs.readFile(storageFile, (err, f) => {
                if (err) {
                    console.error(err);
                }
                storage = JSON.parse(f.toString());

                if (!storage.includes(n)) {
                    storage.push(n);
                    fs.writeFileSync(storageFile, JSON.stringify(storage));
                    (announcementsChannel as TextChannel).send(`${n} days until September 11, 2019, when Zaid's PC should have been sold.`);
                } else {
                    console.log("Countdown already sent, no need.");
                }

                let botUser = bot.user;

                g.setName(`${n} days`)
                    .then()
                    .catch(e => {
                        console.error(`Error setting server name: ${g.name}\n${e}`);
                    });
                g.members.cache.forEach(m => {
                    const match = /-?[0-9]* days/i;
                    if (match.test(m.displayName)) {
                        m.setNickname(`${n} days`).catch(e => console.error(`Error setting nickname: ${m.user.username}#${m.user.discriminator}\n${e}`));
                    }
                });
            });
        });
    }

    function setup() {
        let now = new Date();
        let day = now.getDay();
        let millisTill12pm = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 12, 0, 0, 0).getMilliseconds() - now.getMilliseconds() + 10000;

        if (millisTill12pm > 0) {
            setTimeout(() => {
                const guildMembersSent = [];

                setTimeout(setup, 1000);
            }, millisTill12pm);
        }
    }

    setup();
    run();
};
