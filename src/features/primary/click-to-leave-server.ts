import { bot } from "../..";
import { util } from "discord.js-commando";
import { createChannelIfNotExist } from "../../utils/util";

export default function() {
    bot.on("voiceStateUpdate", (oldS, s) => {
        if (s.channel == null) return;

        if (s.channel.name.toLowerCase() == "Click To Leave Server".toLowerCase() || s.channel.name.toLowerCase() == "CTLS".toLowerCase() || s.channel.name.toLowerCase() == "Click Leave Server".toLowerCase()) {
            let afkChannelPromise = createChannelIfNotExist(s.guild.id, "AFK", {type: 'voice'});

            afkChannelPromise.then(afkChannel => {
                afkChannel
                    .createInvite({
                        maxUses: 1
                    })
                    .then(invite => {
                        // On success of creating the invite, send it
                        s.member.send(`Here's an invite link for you if you clicked that by accident: ${invite.url}`).then(inviteMsg => {
                            // On success of sending the invite, kick them
                            s.kick(s.member.displayName + ' clicked "Click To Leave Server"').catch(() => {
                                // On failure to kick them, delete the message
                                inviteMsg.delete();
                                s.member.send(`The bot doesn't have enough permissions to kick you from \`${s.guild.name}\`.`);
                                s.setChannel(afkChannel);
                            });
                        });
                    });
            });
            // Create the invite
        }
    });
};
