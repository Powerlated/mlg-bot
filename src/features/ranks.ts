
import * as low from "lowdb";
import { UserRank } from "../commands/util/rankmanager";
import { bot } from "..";
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("ranks.json");
const db = low(adapter);
const romanize = require("romanize");

export default {
    db,
    /**
     *
     * @param {string} guildId
     * @param {string} roleId
     * @param {number} rankLevel
     */
    addRank(guildId, roleId, rankLevel) {
       
        

        return new Promise(async (resolve, reject) => {
            var guild = bot.guilds.cache.get(guildId);

            if (guild == null) {
                reject("the bot isn't added to guild or guild doesn't exist");
                return;
            }

            if (guild.roles.cache.get(roleId) == null) {
                reject(`role id ${roleId} does not exist in ${guild.name} (${guild.id})`);
                return;
            }

            if (!(await db).has(guildId).value()) {
                (await db).set(guildId, {}).write();
            }

            var dbGuild = (await db).get(guildId);

            if (!dbGuild.has("roles").value()) {
                // @ts-ignore
                dbGuild.set("roles", {}).write();
            }

            // @ts-ignore
            var dbGuildRoles = dbGuild.get("roles");

            Object.entries(dbGuildRoles.value() as UserRank[]).forEach(([key, value]) => {
                if (value.rankLevel == rankLevel) {
                    reject(`<@&${key}> has already been assigned the rank level of \`${value.rankLevel}\`.`);
                }
            });

            if (!dbGuildRoles.has(roleId).value()) {
                dbGuildRoles.set(roleId, {}).write();
                var dbGuildRole = dbGuildRoles.get(roleId);
                dbGuildRole.set("rankLevel", rankLevel).write();
                resolve();
            } else {
                reject("that rank number already exists.");
            }

            (await db).write();
        });
    },
    /**
     *
     * @param {string} guildId
     * @param {string} roleId
     */
    removeRank(guildId, roleId) {
       
        

        return new Promise(async (resolve, reject) => {
            let dbGuild = (await db).get(guildId);

            if (!dbGuild.value()) {
                reject("guild does not exist.");
                return;
            }

            // @ts-ignore
            let dbGuildRoles = dbGuild.get("roles");
            let dbGuildRole = dbGuildRoles.get(roleId);

            if (!dbGuildRoles.has(roleId).value()) {
                reject("role does not have a rank level.");
            } else {
                dbGuildRoles.unset(roleId).write();
                resolve();
            }
        });
    },
    /**
     *
     * @param {string} guildId
     */
    getGuildRoles(guildId) {
       
        

        return new Promise(async (resolve, reject) => {
            let dbGuild = (await db).get(guildId);

            if (!dbGuild.value()) {
                reject("guild is not tracked by MLG Bot. (Try adding a role to Rank Manager.)");
            }

            // @ts-ignore
            resolve(dbGuild.get("roles").value());
        });
    },
    /**
     *
     * @param {string} guildId
     * @param {import('discord.js').GuildMember} target
     * @param {boolean} up
     *
     * @returns {Promise<[any, import('discord.js').GuildMember]>}
     */
    changeRank(guildId, target, up) {
       
        

        return new Promise((resolve, reject) => {
            let guild = bot.guilds.cache.get(guildId);
            let rankMasterList = [];
            this.getGuildRoles(guildId)
                .then(roles => {
                    let sortedRoles = Object.entries(roles as UserRank[]).sort((_a, _b) => {
                        return _a[1].rankLevel - _b[1].rankLevel;
                    });

                    let userHasRoles = sortedRoles.filter(v => target.roles.has(v[0]));
                    let userNoHasRoles = sortedRoles.filter(v => !target.roles.has(v[0]));
                    let lastRole = userHasRoles[userHasRoles.length - 1][0];
                    let lastRoleRank = sortedRoles.filter(v => v[0] == lastRole)[0][1].rankLevel;
                    let nextRole = sortedRoles.filter(v => v[1].rankLevel >= userNoHasRoles[0][1].rankLevel)[0][0];
                    let nextRoleRank = sortedRoles.filter(v => v[0] == nextRole)[0][1].rankLevel;

                    Promise.all([target.removeRole(lastRole), target.addRole(nextRole)])
                        .then(() => resolve([target.roles.get(nextRole), target]))
                        .catch(() => reject(`failed to rank ${up ? "up" : "down"} ${target} from <@&${lastRole}> (Rank ${lastRoleRank}) to <@&${nextRole}>  (Rank ${nextRoleRank}). (Maybe bot doesn't have permission)?`));
                })
                .catch(console.log);
        });
    }
};
