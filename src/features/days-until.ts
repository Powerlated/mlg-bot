export default function main () {
  let today = new Date();
  var cmas = new Date(today.getFullYear(), 8, 11);
  if (today.getMonth() == 8 && today.getDate() > 11) {
    cmas.setFullYear(cmas.getFullYear());
  }
  var one_day = 1000 * 60 * 60 * 24;
  return (Math.ceil((cmas.getTime() - today.getTime()) / (one_day)));
};