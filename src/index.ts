import * as Commando from "discord.js-commando";
import * as Discord from "discord.js";
import { GuildChannel } from "discord.js";
import { createChannelIfNotExist } from "./utils/util";
import setupFeatures from "./feature-setup";

const auth = require("../auth.json");
const path = require("path");
//const sqlite = require("sqlite");
const logger = require("winston");
//const util = require('./utils/util');

export const commandPrefix = "#!mlg";
export const owner = "190195717531238400"; // MLGxPwnentz#1728
export const bestGuildId = "435911907526836224";

var bestGuildOutputChannel;

export const datadir = __dirname + "/../data/";

// Add a require handler for text files

import BitmojiCommand from "./commands/fun/bitmoji";
import TicTacToeStartCommand from "./commands/games/tic-tac-toe-start";
import RolesCommand from "./commands/util/roles";
import EveryoneCommand from "./commands/util/everyone";
import ProfilePictureCommand from "./commands/util/profilepicture";
import RankManagerCommand from "./commands/util/rankmanager";
import HereCommand from "./commands/fun/here";
import RandomEmojisCommand from "./commands/fun/randomemojis";
import ReeCommand from "./commands/fun/ree";



// Setup the logger
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console(), {
    colorize: true
});

logger.level = "debug";

// Setup database
require("./utils/db");

/** @type {Discord.Client} */
export var bot = new Commando.CommandoClient({
    commandPrefix: commandPrefix,
    owner: owner
});

//var database = sqlite.open(path.join(__dirname, "mlg-bot.db"));
//var databaseProvider = database.then(db => new commando.SQLiteProvider(db));

// Set up and open a database
//bot.setProvider(databaseProvider);



bot.registry
    .registerGroup({ id: "garbage", name: "commands that act like and are garbage" })
    .registerGroup({ id: "fun", name: "Fun" })
    .registerGroup({ id: "games", name: "Games" })
    .registerDefaults()
    .registerCommands([
        BitmojiCommand,
        HereCommand,
        RandomEmojisCommand,
        ReeCommand,
        TicTacToeStartCommand,
        RolesCommand,
        EveryoneCommand,
        ProfilePictureCommand,
        RankManagerCommand,
    ]);

const allowedCommands = ["help", "sendwednesday", "profilepicture"];

// @ts-ignore
bot.dispatcher.addInhibitor(msg => {
    if (msg.channel.type == "dm" && msg.command && !allowedCommands.includes(msg.command.name)) {
        msg.reply(`You can only use \`${allowedCommands.join("|")}\` in DMs with this bot.`);
        return `unallowed`
    }

    if ((msg.channel as GuildChannel).name != "bot-commands" && msg.channel.type != "dm") {
        msg.reply("please use bot commands in `bot-commands`. Thank you.")
            // Delete the bot's message after 5 seconds
            .then(botMsg => {
                if (botMsg instanceof Discord.Message) {
                    botMsg.delete({ timeout: 5000 });
                }
            });
        msg.delete();

        return `unallowed`;
    }

    return;
});


// When the bot is ready, display some info
bot.on("ready", evt => {
    logger.info("Connected");
    logger.info("Logged in as: ");
    logger.info(bot.user.username + " - (" + bot.user.id + ")");

    bot.user.setActivity(`Say "${commandPrefix} help"`, {
        type: "PLAYING"
    });

    // bot.user.setUsername("MLG Bot");

    const glob = require("glob"),
        path = require("path");
    glob.sync("./src/features/primary/*.js").forEach(file => {
        console.log(`Loaded feature from ${file}`);
        require(path.resolve(file))();
    });
    //bestGuildOutputChannel = util.getChannelFromGuild(bestGuildId, 'bot-output');
});

// Do something when a message is sent
bot.on("message", msg => {
    if (msg.author.id != bot.user.id && msg.channel.type == "text") {
        createChannelIfNotExist(msg.guild.id, "bot-commands", { type: "text" });
        createChannelIfNotExist(msg.guild.id, "bot-output", { type: "text" });

    }

    if (msg.channel.type == "dm") {
        return;
    }

    var roles_theNormies = msg.guild.roles.cache.get("The Normies");

    // If they are not the bot, do something
    if (msg.author.username != bot.user.username) {
        // Display message info
        /*  logger.info(msg.author.id + " (" + msg.author.username + ') sent the message "' + msg.content + '"');
        logger.info("in the server " + msg.guild.id + " (" + msg.guild.name + ").");
        logger.info("");
 */
        // Replies to that user id with "begone thot"
        if (msg.author.id == "265956755555483650") {
            msg.reply("begone thot");
        } else if (roles_theNormies != null && msg.member.roles.cache.get(roles_theNormies.id)) {
            // Replies to anyone in a group called "The Normies"
            msg.reply("get off my server you normie");
        }

        if (msg.content.includes("69") || msg.content.includes("420")) {
            msg.channel.send(`<@${msg.author.id}> Nice.`).then(msg => msg.delete({ timeout: 5000 }));
        }
    }
});

// Login the bot
bot.login(auth.token);

setupFeatures();