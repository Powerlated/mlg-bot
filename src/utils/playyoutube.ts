const fs = require("fs");
import * as ytdl from "ytdl-core";
import { VoiceConnection } from "discord.js";

const streamOptions = {
  seek: 0,
  volume: 1
};

const dir = "./tmp/";

function parseYouTube(url: string) {
  var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  var match = url.match(regExp);
  return match && match[7].length == 11 ? match[7] : false;
}

function play(connection: VoiceConnection, input: string) {
  new Promise(function (resolve, reject) {
    const dispatcher = connection.play(input);

    dispatcher.on("end", reason => {
      connection.disconnect();
      resolve();
    });
  });
}

export default function playYouTube(url: string, connection: VoiceConnection) {
  const parsedUrl = parseYouTube(url);

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  if (!fs.existsSync(dir + parsedUrl)) {
    const stream = ytdl(url, {
      filter: "audio"
    });
    var writeStream;
    stream.pipe((writeStream = fs.createWriteStream(dir + parsedUrl)));
    writeStream.on("close", () => {
      play(connection, dir + parsedUrl);
    });
  } else {
    play(connection, dir + parsedUrl);
  }
};
