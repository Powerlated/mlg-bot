import { GuildChannel, User } from 'discord.js';
import * as fs from 'fs';
import { bot, datadir } from '..';
import { GuildCreateChannelOptions } from 'discord.js';
const imageDownload = require('image-download');
const shajs = require('sha.js');

/** @returns {import('discord.js').GuildChannel} */
export function getChannelFromGuild(guildId, channelName, channelType: any = 'text', msg?) {
	const guild = bot.guilds.cache.get(guildId);

	let channel;
	guild.channels.cache.forEach((c) => {
		if (c.type == channelType && c.name == channelName) {
			channel = c;
		}
	});

	if (channel == null && msg != null) {
		msg.reply(`there is no \`${channelName}\` channel in the server. Maybe ask the owner to add one?`);
	}

	return channel;
}
export function sendToOutputChannel(guildId, text, options, msg) {
	var outputChannel = getChannelFromGuild(guildId, 'bot-output');

	if (!(outputChannel == null)) {
		return outputChannel.send(text, options);
	}
}
export function downloadImg(url) {
	let promise = new Promise((resolve: any, reject) => {
		const dir = 'downloads/';

		if (!fs.existsSync(dir)) {
			fs.mkdirSync(dir);
		}

		imageDownload(url)
			.then((buffer) => {
				const dest = dir + shajs('sha256').update(buffer).digest('hex').substring(0, 8);
				fs.writeFileSync(dest, buffer);
				resolve(dest, dir);
			})
			.catch((err) => {
				reject(err);
			});
	});
	return promise;
}
export async function createChannelIfNotExist(guildId, channelName, opts: GuildCreateChannelOptions): Promise<GuildChannel> {
	let storageFile = datadir + "createChannelIfNotExist.json";

	if (bot.guilds.cache.has(guildId)) {
		if (!getChannelFromGuild(guildId, channelName, opts.type)) {
			return bot.guilds.cache.get(guildId).channels.create(channelName, opts);
		} else {
			return new Promise((resolve, reject) => {
				resolve(getChannelFromGuild(guildId, channelName, opts.type));
			});
		}
	}
}
/** 
* @param searchString Either a valid regex or user ID 
* @returns {Promise} A list of found users, or an error message if something goes wrong.
*/
export function findUsers(searchString): Promise<User[]> {
	return new Promise((resolve, reject) => {
		const bot = require('..').bot;
		let snowflake = false;
		let foundGuildMembers = [];
		let foundGuildMembersIDs = [];
		if (new RegExp(/^[0-9]{18}$/i).test(searchString)) {
			snowflake = true;
			bot.guilds.forEach((guild) => {
				guild.members.forEach((guildMember) => {
					if (
						!guildMember.user.bot &&
						guildMember.id == searchString &&
						!foundGuildMembersIDs.includes(guildMember.id)
					) {
						foundGuildMembers.push(guildMember);
						foundGuildMembersIDs.push(guildMember.id);
					}
				});
			});
		} else if (searchString && searchString.length > 0) {
			try {
				let regex = new RegExp(searchString, 'i');
				bot.guilds.forEach((guild) => {
					guild.members.forEach((guildMember) => {
						if (
							!guildMember.user.bot &&
							regex.test(guildMember.displayName) &&
							!foundGuildMembersIDs.includes(guildMember.id)
						) {
							foundGuildMembers.push(guildMember);
							foundGuildMembersIDs.push(guildMember.id);
						}
					});
				});
			} catch (e) {
				reject('Invalid regex.');
			}
		}
		if (foundGuildMembers.length > 0) {
			resolve(foundGuildMembers);
		} else {
			reject(`That ${snowflake ? 'Snowflake ID' : 'regex'} did not resolve to a user.`);
		}
	});
}
