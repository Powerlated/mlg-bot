import * as low from "lowdb";
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync(process.cwd() + "/data/db.json");
const db = low(adapter);

// @ts-ignore
db.defaults({}).write();

export default {
    /**
     *  @param {string} key
     *  @param {object} data
     *  @returns {void}
     */
    async set(key, data) {
        (await db).set(key, data).write();
    },
    /**
     *  @param {string} key
     *  @returns {object}
     */
    async get(key) {
        return (await db).get(key).value();
    }
};
