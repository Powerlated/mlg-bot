import * as Commando from 'discord.js-commando';
import TicTacToeGame from '../../features/games/tic-tac-toe';
import { createChannelIfNotExist } from '../../utils/util';

/** Key is a channel, value is a TicTacToeGame */
var ongoingGames = new Map();

export default class TicTacToeStartCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: "tictactoe",
            aliases: ["ttc"],
            group: "games",
            memberName: "tictactoe",
            description: "Start a game of tic-tac-toe in `tic-tac-toe`.",
            examples: ["tictactoe"],
            args: [{ key: "size", prompt: "What size do you want your board to be on each side?", default: 3, type: 'integer' }]
        });
    }

    async run(msg, args) {
        createChannelIfNotExist(msg.guild.id, "tic-tac-toe", {type: "text"}).then(channel => {
            if (channel != null && !ongoingGames.has(channel)) {
                ongoingGames.set(channel, true);
                const game = new TicTacToeGame(channel, msg.author, ongoingGames).start();
            } else {
                msg.reply("there is already a game going on. Please wait.");
            }
        });

        return msg;
    }
};
