import * as Commando from "discord.js-commando";

export default class HereCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: "here",
            group: "fun",
            description: "Join the user's channel.",
            guildOnly: true,
            memberName: "here"
        });
    }

    /**
     * @param {import('discord.js-commando').CommandMessage} msg
     * @param {string[]} args
     */
    async run(msg, args) {
        let author = msg.guild.member(msg.author);
        if (author.voiceChannel) author.voiceChannel.join();
        
        return msg;
    }
};
