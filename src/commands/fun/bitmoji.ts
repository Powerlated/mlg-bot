const libmoji = require('libmoji');
import * as Commando from 'discord.js-commando';
import * as Discord from 'discord.js';

export const randomElement = (array: any): any => {
	return array[Math.floor(Math.random() * array.length)];
};

export default class BitmojiCommand extends Commando.Command {
	constructor(client) {
		super(client, {
			name: 'bitmoji',
			aliases: ['bm'],
			group: 'fun',
			memberName: 'bitmoji',
			description: 'Generate a bitmoji.',
			argsType: 'multiple',
			examples: ['#!mlg bm [gender]']
		});
	}

	/**
   *
   * @param {cmdMsg} msg
   * @param {Array} args
   */
	async run(msg, args) {
		const s = msg.channel.send;
		if (args[0] != undefined && args[0].toLowerCase() == 'list') {
			const genders = [];
			libmoji.genders.forEach((genderMap) => {
				genders.push(genderMap[0]);
			});

			const gendersCapitalized = [];
			for (let gender of genders) {
				gender = gender.charAt(0).toUpperCase() + gender.substr(1);
				gendersCapitalized.push(gender);
			}

			const posesCapitalized = [];
			for (let pose of libmoji.poses) {
				pose = pose.charAt(0).toUpperCase() + pose.substr(1);
				posesCapitalized.push(pose);
			}

			const styles = [];
			libmoji.styles.forEach((genderMap) => {
				styles.push(genderMap[0]);
			});

			msg.channel.send(`
		  \`\`\`
		  A list of the basic 3 Bitmoji traits:\n
		  Genders: ${gendersCapitalized.join(', ')}\n
		  Poses:   ${posesCapitalized.join(', ')}\n
		  Styles:  ${styles.join(', ')}
		  \`\`\`
		  `);
		}
		// else if (
		// 	(args[0].toLowerCase() == 'getbrandsfromgender' || args[0].toLowerCase() == 'gbfg') &&
		// 	args.length == 2
		// ) {
		// 	const gender = args[1].toLowerCase();
		// 	if (gender == 'male' || gender == 'female') {
		// 		const brandGenderData = libmoji.getBrands(gender);
		// 		const brandGenderDataList = [];
		// 		brandGenderData.forEach((data) => {
		// 			brandGenderDataList.push(data.name);
		// 		});
		// 		msg.reply(
		// 			`the brands available for the gender ${ucFirst(gender)} are ${brandGenderDataList.join(', ')}`
		// 		);
		// 	} else {
		// 		msg.reply('please select a gender that is recognized by Bitmoji.');
		// 	}
		// } else if (
		// 	(args[0].toLowerCase() == 'getoutfitsforbrand' || args[0].toLowerCase() == 'gofb') &&
		// 	args.length == 2
		// ) {
		// 	const brand = ucFirst(args[1].toLowerCase());
		// 	const brandOutfitData = libmoji.getOutfits(libmoji.getBrands());

		// 	if (brandOutfitData == undefined) {
		// 		msg.reply(`the brand ${brand} does not exist, and therefore, no outfits have been listed.`);
		// 		return;
		// 	}

		// 	const brandOutfitDataList = [];
		// 	brandOutfitData.forEach((data) => {
		// 		brandOutfitDataList.push(data.name);
		// 	});
		// 	if (brandOutfitDataList != 0) {
		// 		msg.reply(
		// 			`the outfits available for the brand ${ucFirst(brand)} are ${brandOutfitDataList.join(', ')}`
		// 		);
		// 	}
		// }
		let gender;
		if (args[0] == 'male' || args[0] == 'female') {
			gender = args[0];
		} else {
			gender = randomElement(['male', 'female']);
		}

		let rotation = randomElement([0, 1, 7]);

		const pose = libmoji.poses.randomElement();
		const style = randomElement(['bitstrips', 'bitmoji', 'cm']);
		const brand = libmoji.randBrand(libmoji.getBrands(gender));
		const outfit = libmoji.randOutfit(libmoji.getOutfits(brand));
		const traits = libmoji.randTraits(libmoji.getTraits(gender, style));

		var genderValue;
		if (gender == 'male') genderValue = 1;
		else if (gender == 'female') genderValue = 2;

		var styleValue;
		if (style == 'bitstrips') styleValue = 1;
		else if (style == 'bitmoji') styleValue = 4;
		else if (style == 'cm') styleValue = 5;

		const url = libmoji.buildUrl(pose, 1, genderValue, styleValue, rotation, traits, outfit);
		msg.reply(url);

		return msg;
	}
};

function ucFirst(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}
