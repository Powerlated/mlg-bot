import * as Commando from "discord.js-commando";
import { bot } from "../..";
import playYouTube from "../../utils/playyoutube";
import { GuildChannel, VoiceChannel } from "discord.js";

const oneLine = require("common-tags").oneLine;
const logger = require("winston");
const maxTryCount = 10;

export default class ReeCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "ree",
      aliases: ["reee", "reeee"],
      group: "fun",
      memberName: "ree",
      description: "Join a random channel with people in it and ree.",
      details: oneLine`
                This amazing command just joins a random voice channel with people in it.
            `,
      examples: ["ree"]
    });
  }

  // @ts-ignore
  async run(msg: Commando.CommandoMessage, args: string[]) {
    var lg;
    var d = Math.random();
    if (d > 0.8) {
      lg = true;
    } else {
      lg = false;
    }

    var channel: VoiceChannel;
    var channelFound = false;
    var tries = 0;
    var videoURL = "https://www.youtube.com/watch?v=7cmexEZGQbs";
    var lgVideoURL = "https://www.youtube.com/watch?v=XvOun1ilT4o";

    var peopleAvailable = false;

    var guildChannels = msg.guild.channels.cache.values();

    for (let guildChannel of guildChannels) {
      if (guildChannel.type == "voice" && guildChannel.members.size != 0) {
        peopleAvailable = true;
      }
    }

    if (!peopleAvailable) {
      msg.reply("there are no people in voice channels you idiot!");
      return;
    }

    while (!channelFound) {
      var channels = msg.guild.channels;
      var channelsArray = channels.cache.array();
      let candidate = channelsArray[Math.floor(Math.random() * channelsArray.length)];
      if (candidate instanceof VoiceChannel) {
        channel = candidate;
      }

      var skip = false;

      if (channel.members == null) skip = true;

      if (
        !skip &&
        channel.members.has(bot.user.id) &&
        channel.members.size == 1
      )
        skip = true;

      if (!skip) {
        if (channel.members.size != 0 && channel.type == "voice")
          channelFound = true;
        logger.info(channel.members.size);
      }
    }

    if (channelFound) {
      var url;
      if (lg) {
        url = lgVideoURL;
      } else {
        url = videoURL;
      }
      channel
        .join()
        .then(connection => {
          playYouTube(url, connection);
          bot.on("voiceStateUpdate", (oldMember, newMember) => {
            if (connection.channel.members.size == 1) {
              connection.disconnect();
            }
          });
        })
        .catch(reason =>
          msg.reply(`an error occured, here is the reason: ${reason}`)
        );

      var reeing = "reeing";
      if (lg) {
        reeing = "LGing";
      }
      msg.reply(`the ${reeing} is commencing in the channel \`${channel.name}\`.`);
    } else {
      msg.reply(
        `no applicable channel has been found after ${maxTryCount} tries.`
      );
    }
  }
};
