import * as Commando from "discord.js-commando";
import { sendToOutputChannel } from "../../utils/util";
const oneLine = require("common-tags").oneLine;
const randomEmojis = require("random-unicode-emoji");
const truncate = require("truncate-utf8-bytes");

export default class RandomEmojisCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "randomemojis",
      aliases: ["re"],
      group: "fun",
      memberName: "randomemojis",
      description: "Message emojis.",
      details: oneLine`
                Return number of specified emojis.
            `,
      examples: ["randomemojis 1000"],
      argsCount: 1
    });
  }

  async run(msg, args) {
    if (args % 1 != 0) {
      msg.reply("<count> has to be an integer.");
      return;
    }

    var emojis =
      "Emojis: " + randomEmojis.random({ count: parseInt(args) }).join("");
    var truncatedEmojis = truncate(emojis, 2000);

    // Remove newlines and send
    sendToOutputChannel(msg.guild.id, truncatedEmojis, {}, msg);

    return msg;
  }
};
