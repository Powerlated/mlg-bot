//@ts-check

import * as Commando from "discord.js-commando";
import * as logger from "winston";
import ranks from "../../features/ranks";

export type UserRank = {
    rankLevel: number;
};

export default class RankManagerCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: "rankmanager",
            aliases: ["rm"],
            group: "util",
            argsType: "multiple",
            memberName: "rankmanager",
            description: "Command for managing the MLG Bot Rank System",
            argsPromptLimit: 1
        });
    }

    async run(msg, args) {
        if (args[0] == "add") {
            try {
                var roleId = msg.guild.roles.find(role => role.name.toLowerCase() === args[1].toLowerCase()).id;
            } catch (e) {
                if (args[1]) msg.reply(`"${args[1]}" not found`);
                else msg.reply("please specify the role to add, surrounded by double quotes if the role name has spaces.");
                return;
            }
            this.add(msg, args, roleId);
        } else if (args[0] == "remove") {
            try {
                var roleId = msg.guild.roles.find(role => role.name.toLowerCase() === args[1].toLowerCase()).id;
            } catch (e) {
                if (args[1]) msg.reply(`"${args[1]}" not found`);
                else msg.reply("please specify the role to add, surrounded by double quotes if the role name has spaces.");
                return;
            }
            this.remove(msg, args, roleId);
        } else if (args[0] == "list") {
            this.list(msg, args, roleId);
        } else if (args[0] == "rankup") {
            this.rankup(msg, args, roleId);
        } else if (args[0] == "rankdown") {
            this.rankdown(msg, args, roleId);
        } else {
            msg.reply(`
\`\`\`
--- Rank Manager ---
Rank Manager is a system for mangaging and assigning ranks in a Discord server.

#!mlg rm <add|remove|list|rankup|rankdown> 

#!mlg rm add <role name> <rank number>
    * Adds <role name> to RM with rank <rank number>.
#!mlg rm remove <role name>
    * Removes <role name> from RM.
#!mlg rm list
    * Lists all registered ranks in RM.
#!mlg rm rankup <@TargetMention>
    * Ranks someone up in RM.
#!mlg rm rankdown <@TargetMention>
    * Ranks someone down in RM.
\`\`\`
            `);
        }

        return msg;
    }

    /**
     * @param {import("discord.js-commando").CommandMessage} msg
     * @param {any[]} args
     * @param {string} roleId
     */
    add(msg, args, roleId) {
        if (args.length == 3) {
            if (isNaN(args[2])) {
                msg.reply("rank number has to be an integer.");
                return;
            }

            ranks
                .addRank(msg.guild.id, roleId, args[2])
                .then(() => {
                    msg.reply(`successfully set <@&${roleId}> to be rank ${args[2]}!`);
                })
                .catch(err => {
                    msg.reply(err);
                });
        } else {
            msg.reply("the format is: `#!mlg rm add <role name> <rank number>`");
        }
    }

    /**
     * @param {import("discord.js-commando").CommandMessage}  msg
     * @param {any[]} args
     * @param {string} roleId
     */
    remove(msg, args, roleId) {
        if (args.length == 2) {
            ranks
                .removeRank(msg.guild.id, roleId)
                .then(() => {
                    msg.reply(`successfully removed <@&${roleId}>'s rank level.`);
                })
                .catch(err => {
                    msg.reply(err);
                });
        } else {
            msg.reply("the format is: `#!mlg rm remove <role name>`");
        }
    }

    /**
     * @param {import("discord.js-commando").CommandMessage} msg
     * @param {any[]} args
     * @param {string} roleId
     */
    list(msg, args, roleId) {
        ranks
            .getGuildRoles(msg.guild.id)
            .then(roles => {
                let finalMsg = "";

                let arr = Object.entries(roles as UserRank[]).sort((_a, _b) => {
                    return _a[1].rankLevel - _b[1].rankLevel;
                });
                arr.forEach(([key, value]) => {
                    finalMsg += `<@&${key}>: ${value.rankLevel}\n`;
                });
                msg.channel.send(finalMsg);
            })
            .catch(err => msg.reply(err));
    }

    /**
     * @param {import("discord.js-commando").CommandMessage} msg
     * @param {any[]} args
     * @param {string} roleId
     */
    async rankup(msg, args, roleId) {
        if (args[1]) {
            var targetMention = args[1];
            var targetId = targetMention.substring(2).slice(0, -1);
            ranks
                .changeRank(msg.guild.id, await msg.guild.fetchMember(targetId), true)
                .then(([role, target]) => {
                    msg.channel.send(`<@${target.id}> has been promoted to <@&${role.id}> by <@${msg.author.id}>!`);
                })
                .catch(err => msg.reply(err));
        } else {
            msg.reply("the format is: `#!mlg rm rankup @TargetMention`");
        }
    }

    /**
     * @param {import("discord.js-commando").CommandMessage} msg
     * @param {any[]} args
     * @param {string} roleId
     */
    async rankdown(msg, args, roleId) {
        if (args[1]) {
            var targetMention = args[1];
            var targetId = targetMention.substring(2).slice(0, -1);
            ranks
                .changeRank(msg.guild.id, await msg.guild.fetchMember(targetId), false)
                .then(([role, target]) => {
                    msg.channel.send(`<@${target.id}> has been demoted to <@&${role.id}> by <@${msg.author.id}>.`);
                })
                .catch(err => msg.reply(err));
        } else {
            msg.reply("the format is: `#!mlg rm rankdown @TargetMention`");
        }
    }
};
