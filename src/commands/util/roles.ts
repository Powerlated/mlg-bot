// @ts-check

import db from "../../utils/db";
import * as Commando from "discord.js-commando";
import { Message, Role } from "discord.js";

export default class RolesCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: "roles",
            group: "util",
            memberName: "roles",
            description: "Set roles.",
            argsType: "multiple"
        });
    }

    async run(msg: Commando.CommandoMessage, args: string[]) {
        let roles = await db.get(`roles${msg.guild.id}`);

        let guildRoles = msg.guild.roles.cache;
        guildRoles = guildRoles.sort((a, b) => a.position - b.position);
        if (args[0] == "reset") {
            if (msg.member.hasPermission("ADMINISTRATOR")) {
                db.set(`roles${msg.guild.id}`, null);
                msg.say("Done. Roles for this server reset. Use `#!mlg roles` to reinitialize roles in the future.");
            } else {
                msg.say(`Cannot reset roles as you, ${msg.author}, do not have Administrator.`);
            }
            return;
        }

        if (!roles) {
            if (msg.member.hasPermission("ADMINISTRATOR")) {
                if (args.length == 0) {
                    msg.say("This server has not been initialized with roles yet.");
                    let s = "__**Roles**__\n";
                    guildRoles.forEach((role, string) => {
                        s += `**${role.position + 1}** ` + role.toString() + "\n";
                    });
                    msg.say(s);
                    msg.say("Enter the list of assignable roles after the command.");
                    return msg.say("Example: `#!mlg roles 1 4 2 5 14 42`");
                } else {
                    if (args.length > 0) {
                        msg.channel.send("You have chosen the roles:");
                        let chosenRoles: Role[] = [];
                        args.forEach(arg => {
                            let roleA = Array.from(
                                guildRoles
                                    .filter(role => {
                                        return (role.position + 1).toString() == arg;
                                    })
                                    .entries()
                            )[0];
                            if (roleA && !roleA[1]) {
                                msg.channel.send(`**${arg}** is an invalid role index. Please try again.`);
                            } else {
                                chosenRoles.push(roleA[1]);
                            }
                        });
                        let f = "";
                        let fRolesIds = [];

                        chosenRoles.forEach(role => {
                            f += `**${role.position + 1}** ` + role.toString() + "\n";
                            fRolesIds.push(role.id);
                        });
                        msg.channel.send(f);
                        db.set(`roles${msg.guild.id}`, fRolesIds);
                        msg.channel.send("Done. Allowed roles have been set. To set roles in the future, use `#!mlg roles reset`.");
                    }
                }
            } else {
                msg.reply("this server has not been initialized with roles yet. Ask an admin to run this command.");
            }
        } else {
            let allowedRoles = guildRoles.filter((role, key, c) => {
                return roles.filter(v => role.id == v).length > 0;
            });
            if (args.length == 0) {
                msg.channel.send("Use this tool to choose roles you want.");
                msg.channel.send("Enter the list of roles you want.");
                msg.channel.send("Example: `#!mlg roles 1 4 2 5 14 42`");
                msg.channel.send(
                    (function () {
                        let s = "__**Roles**__\n";
                        allowedRoles.forEach((role, string) => {
                            s += `**${role.position + 1}** ` + role + "\n";
                        });
                        return s;
                    })()
                );
            } else {
                let error = false;

                /** @type {Discord.Role[]} */
                let chosenRoles = [];
                args.forEach(arg => {
                    let roleA = Array.from(
                        guildRoles
                            .filter(role => (role.position + 1).toString() == arg).entries()
                    )[0];

                    if (roleA && roleA[1] && allowedRoles.has(roleA[1].id)) {
                        chosenRoles.push(roleA[1]);
                    } else {
                        msg.channel.send(`**${arg}** is an invalid role index. Please try again.`);
                        error = true;
                    }
                });

                if (!error) {
                    msg.member.roles.remove(allowedRoles).then(() => {
                        if (chosenRoles.length > 0) {
                            msg.channel.send("You have chosen the roles:");
                            msg.member
                                .roles.add(chosenRoles)
                                .then(() => {
                                    let f = "";
                                    chosenRoles.forEach(role => {
                                        f += `**${role.calculatedPosition + 1}** ` + role + "\n";
                                    });
                                    msg.channel.send(f);
                                    msg.channel.send("Done. Allowed roles have been set for you. Others: `#!mlg roles` to pick your roles.");
                                })
                                .catch(e => {
                                    msg.channel.send("There was an error setting your roles.");
                                    msg.channel.send(`\`\`\`${e}\`\`\``);
                                });
                        } else {
                            msg.channel.send("You have chosen no roles, removing all allowed roles from you. `#!mlg roles` to pull up the prompt again.");
                        }
                    });
                }
            }
        }

    }
}
