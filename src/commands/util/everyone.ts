import * as Commando from "discord.js-commando";

const discord = require("discord.js");
const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync("everyone.json");
const db = low(adapter);

db.defaults({ servers: {} })
  .write();

function initialize(dbObject, name) {
  if (!dbObject.has(name).value()) {
    dbObject.set(name, {}).write();
  }
  return dbObject.get(name);
}

export default class EveryoneCommand extends Commando.Command {
  constructor(client) {
    super(client, {
      name: "everyone",
      aliases: ["e"],
      group: "util",
      memberName: "everyone",
      description: "Mention @everyone using the Everyone Credit System"
    });
  }

  /** @param {discord.Message} */
  async run(msg, args) {
    var dbServers = db.get("servers");
    var dbServer = initialize(dbServers, msg.guild.id);
    var dbServerUsers = initialize(dbServer, 'users');
    var dbServerUser = initialize(dbServerUsers, msg.author.id);

    if (args != "") {
      var currentTime = Math.floor(Date.now() / 1000);

      var lastUsedEveryone = dbServerUser.get('lastUsedEveryone').defaults(new Number(0)).value();

      if ((currentTime - 300 > lastUsedEveryone) || isNaN(lastUsedEveryone)) {
        dbServerUser.set('lastUsedEveryone', currentTime).write();
        msg.channel.send("@everyone " + args);
      } else {
        msg.reply(`you need to wait ${lastUsedEveryone - (currentTime - 300)} seconds to use \`everyone\` again.`)
          .then(sent => sent.delete(5000));
        msg.delete();
      }
    }
    
    return msg;
  }
};
