import * as Commando from "discord.js-commando";
import * as Discord from "discord.js";
import { findUsers } from "../../utils/util";

export default class ProfilePictureCommand extends Commando.Command {
    constructor(client) {
        super(client, {
            name: "profilepicture",
            aliases: ["pp", "avatar", "profilepic"],
            group: "util",
            memberName: "profilepicture",
            description: "Get the profile picture of users with matching regex or Snowflake ID"
        });
    }

    /** @param {discord.Message} */
    async run(msg, args) {
        const avatarURLs = [];
        findUsers(args)
            .then(users => {
                users.forEach(user => {
                    /** @type {Discord.GuildMember} */
                    const guildMember = user;
                    avatarURLs.push(`${guildMember}: ${guildMember.avatarURL}`);
                });
                msg.reply(avatarURLs.join(`\n`));
            })
            .catch(errMsg => {
                msg.reply(errMsg);
            });

        return msg;
    }
};
