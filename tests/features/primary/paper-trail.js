const rewire = require("rewire");

const PaperTrail_rewire = rewire("../../../src/features/primary/paper-trail");
const fm = PaperTrail_rewire.__get__("fm");
const fu = PaperTrail_rewire.__get__("fu");

beforeEach(() =>  {
    jest.resetModules();
});

test("Return proper format from discord.js GuildMember", () => {
    expect(fm({ user: { username: "testusername", discriminator: "testdescriminator" }, id: "testidhere" })).toBe("testusername#testdescriminator<testidhere>");
});

test("Return proper format from discord.js User", () => {
    expect(fu({ username: "testusername", discriminator: "testdescriminator", id: "testidhere" })).toBe("testusername#testdescriminator<testidhere>");
});
