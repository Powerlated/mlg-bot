export default {
  parserOptions: {
    ecmaVersion: 2018
  },
  rules: {
    semi: ["error", "always"]
  }
};
